# Design System (Admin Menu Link)

## Contents of this file

 - Introduction 
 - Requirements
 - Installation
 - Configuration
 - Maintainers

## Introduction

Allows your design system to be accessed from the admin toolbar.

## Requirements

This module requires Drupal core >= 9.5.

## Installation

Install as you would normally install a contributed Drupal module.

## Configuration

After installing the module, configure the URL to your design system at
`/admin/config/user-interface/settings` and adjust the permission to grant
access to view the design system.

## Maintainers

Current maintainers:
- Chris Snyder - [chrissnyder](https://drupal.org/u/chrissnyder)
