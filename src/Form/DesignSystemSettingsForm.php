<?php

declare(strict_types=1);

namespace Drupal\design_system\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Configure design system settings for this site.
 */
class DesignSystemSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'design_system.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'design_system_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::SETTINGS);

    $form['design_system_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Design System URL'),
      '#default_value' => $config->get('design_system_url') ? static::getUriAsDisplayableString($config->get('design_system_url')) : '',
      '#size' => 80,
      '#maxlength' => 2048,
      '#element_validate' => [
        [
          static::class,
          'validateUriElement',
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config(static::SETTINGS)
      ->set('design_system_url', $form_state->getValue('design_system_url'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Form element validation handler for the url element.
   *
   * Disallows saving inaccessible or untrusted URLs.
   */
  public static function validateUriElement($element, FormStateInterface $form_state, $form): void {
    $uri = static::getUserEnteredStringAsUri($element['#value']);
    $form_state->setValueForElement($element, $uri);

    // If getUserEnteredStringAsUri() mapped the entered value to an 'internal:'
    // URI , ensure the raw value begins with '/', '?' or '#'.
    if (parse_url($uri, PHP_URL_SCHEME) === 'internal'
      && !in_array(
        $element['#value'][0],
        [
          '/',
          '?',
          '#',
        ],
        TRUE)) {
      $form_state->setError($element, new TranslatableMarkup('Manually entered paths should start with one of the following characters: / ? #'));
    }
  }

  /**
   * Gets the URI without the 'internal:' scheme.
   *
   * This transforms any 'internal:' URIs to a displayable string.
   *
   * @param string $uri
   *   The URI to get the displayable string for.
   *
   * @return string
   *   The displayable URI.
   */
  protected static function getUriAsDisplayableString(string $uri): string {
    $scheme = parse_url($uri, PHP_URL_SCHEME);

    // By default, the displayable string is the URI.
    $displayable_string = $uri;

    // A different displayable string is shown in case of an 'internal:' scheme.
    if ($scheme === 'internal') {
      $displayable_string = explode(':', $uri, 2)[1];
    }

    return $displayable_string;
  }

  /**
   * Gets the user-entered string as a URI.
   *
   * This maps strings without a detectable scheme: to 'internal:' URIs.
   *
   * This method is the inverse of ::getUriAsDisplayableString().
   *
   * @param string $string
   *   The user-entered string.
   *
   * @return string
   *   The URI, if a non-empty $uri was passed.
   */
  protected static function getUserEnteredStringAsUri(string $string): string {
    // By default, assume the entered string is a URI.
    $uri = trim($string);

    // Detect a schemeless string, map to 'internal:' URI.
    if (!empty($string) && parse_url($string, PHP_URL_SCHEME) === NULL) {
      $uri = 'internal:' . $string;
    }

    return $uri;
  }

}
