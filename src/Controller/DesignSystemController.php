<?php

declare(strict_types=1);

namespace Drupal\design_system\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Controller for displaying the design system.
 */
class DesignSystemController extends ControllerBase {

  /**
   * Displays the design system in an iframe.
   *
   * @return array
   *   Render array with iframed design system.
   */
  public function displayDesignSystem(): array {
    return [
      '#type' => 'html_tag',
      '#tag' => 'iframe',
      '#attributes' => [
        'width' => '100%',
        'height' => 'auto',
        'seamless' => 'seamless',
        'frameborder' => '0',
        'src' => $this->designSystemUrl(),
        'style' => 'height: calc(100vh - 250px);',
      ],
    ];
  }

  /**
   * Provides the URL to the design system.
   *
   * @return string
   *   The URL.
   */
  protected function designSystemUrl(): string {
    return (string) Url::fromUri($this->config('design_system.settings')->get('design_system_url'))
      ->setAbsolute()
      ->toString();
  }

}
